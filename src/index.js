import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import {createBrowserRouter, Navigate, RouterProvider} from "react-router-dom";
import Login from "./Pages/Login/Login";
import Register from "./Pages/Registration/Register";
import User from "./Pages/User/User";
import ErrorPage from "./Error-page/Error-page";



const router = createBrowserRouter([

    {
        path: "/",
        element: <Navigate to="login"/>,
    },
    {
        path: "/login",
        element: <Login/>,
    },
    {
        path: "/register",
        element: <Register/>,
    },
    {
        path: "/user",
        element: <User/>,
    },
    {
        path: "/",
        element: <Navigate to="login"/>,
        errorElement: <ErrorPage/>,  // new
    },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <App />
      <RouterProvider router={router}/>
  </React.StrictMode>
);


