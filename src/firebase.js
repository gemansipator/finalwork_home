// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {createUserWithEmailAndPassword, signInWithEmailAndPassword, getAuth} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDzk3MgzHdGCZiXQI6cyJ6rnPqwr67mnzo",
    authDomain: "react-firebase-auth-e363e.firebaseapp.com",
    projectId: "react-firebase-auth-e363e",
    storageBucket: "react-firebase-auth-e363e.appspot.com",
    messagingSenderId: "468689189930",
    appId: "1:468689189930:web:abd0d1e7bcc26b7126d753",
    measurementId: "G-C7SG1MWYWF"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
////
export const createUser = async (email, password) => {  //createUser создает пользователя Firebase (и возвращает ответ, содержащий информацию о пользователе)
    return createUserWithEmailAndPassword(getAuth(app), email, password);
}

export const signInUser = async (email, password) => { //signInUser регистрирует существующего пользователя Firebase (и возвращает ответ, содержащий информацию о пользователе).
    return signInWithEmailAndPassword(getAuth(app), email, password);
}